import 'package:get/state_manager.dart';
import 'package:shopgetx/product.dart';
import 'package:shopgetx/services.dart';

class ProductController extends GetxController {
  var productList = List<Product>().obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    var products = await Services.fetchProducts();
    if (products != null) {
      productList.value = products;
    }
  }
}
